﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamScript : MonoBehaviour 

{
	private void update()
	{
		if (Input.GetMouseButtonDown(0)) {
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (ray, out hit, 100000.0f)) {
			if (hit.transform != null) {
				PrintName (hit.transform.gameObject);
			}
		}
	}
	}

	private void PrintName(GameObject go)
	{
		print (go.name);
	}
}
