﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamChangev2 : MonoBehaviour
{
	
	public GameObject Camera1;
	public GameObject Camera2;
	public GameObject Camera3;
	public GameObject Camera4;
	public Texture texture; 

	void Start() 
	{ 
		Camera1.SetActive (true);
		Camera2.SetActive (false);
		Camera3.SetActive (false);
		Camera4.SetActive (false); 
	}

	void OnGUI() {
		if(GUI.Button(new Rect(0, 0, 100, 100), texture)) 
		{
			Camera1.SetActive (true);
			Camera2.SetActive (false);
			Camera3.SetActive (false);
			Camera4.SetActive (false);
		}
		if(GUI.Button(new Rect(100, 0, 100, 100), texture)) 
		{
			Camera1.SetActive (false);
			Camera2.SetActive (true);
			Camera3.SetActive (false);
			Camera4.SetActive (false);
		}
		if(GUI.Button(new Rect(200, 0, 100, 100), texture)) 
		{
			Camera1.SetActive (false);
			Camera2.SetActive (false);
			Camera3.SetActive (true);
			Camera4.SetActive (false);
		}
		if(GUI.Button(new Rect(300, 0, 100, 100), texture)) 
		{
			Camera1.SetActive (false);
			Camera2.SetActive (false);
			Camera3.SetActive (false);
			Camera4.SetActive (true);
		}
}
}






				