Old simply refers to variation02.


The head and eyes use specifically the following textures:

BritishBaseMesh_old_specular
BritishBaseMesh_old_normal
BritishBaseMesh_old_diffuse

The hair uses the following:

BritishBaseMesh_old_hair_diffuse (PNG for the transparency of alpha)
BritishBaseMesh_old_specular or BritishBaseMesh_hair_specular
BritishBaseMesh_old_diffuse or BritishBaseMesh_hair_normal

I recommend using the specific hair texture maps which can be found in the "commontextures"
as these were purposely made for it while the others were a byproduct of the head / eye texture maps.