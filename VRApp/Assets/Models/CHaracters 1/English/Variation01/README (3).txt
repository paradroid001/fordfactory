Young simply refers to variation01.


The head and eyes use specifically the following textures:

BritishBaseMesh_young_specular
BritishBaseMesh_young_normal
BritishBaseMesh_young_diffuse

The hair uses the following:

BritishBaseMesh_young_hair_diffuse (PNG for the transparency of alpha)
BritishBaseMesh_young_specular or BritishBaseMesh_hair_specular
BritishBaseMesh_young_diffuse or BritishBaseMesh_hair_normal

I recommend using the specific hair texture maps which can be found in the "commontextures"
as these were purposely made for it while the others were a byproduct of the head / eye texture maps.