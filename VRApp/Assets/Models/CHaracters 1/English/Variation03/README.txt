Middle simply refers to variation03.


The head and eyes use specifically the following textures:

BritishBaseMesh_middle_specular
BritishBaseMesh_middle_normal
BritishBaseMesh_middle_diffuse

The hair and glasses use the following:

BritishBaseMesh_middle_hair_diffuse (PNG for the transparency of alpha)
BritishBaseMesh_middle_specular or BritishBaseMesh_hair_specular
BritishBaseMesh_middle_diffuse or BritishBaseMesh_hair_normal

I recommend using the specific hair texture maps which can be found in the "commontextures"
as these were purposely made for it while the others were a byproduct of the head / eye texture maps.