﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transparent : MonoBehaviour {
	public Shader Opaque;
	public Shader Trans;
	public Renderer rend;

	void Update ()
	{
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Additive")) {
			print ("additive collider enter");
			rend.material.shader = Trans;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag ("Additive")) {
			print ("additive collider exit");
			rend.material.shader = Opaque;
		}
	}
}
