﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disapear : MonoBehaviour {
	private bool touchingPlayer = false;
	private bool appear = true;

	void Update ()
	{
		if (touchingPlayer)
			this.gameObject.GetComponent<Renderer>().enabled = false;
		if (appear)
			this.gameObject.GetComponent<Renderer>().enabled = true;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Asleep")) {
			print ("collider enter");
			touchingPlayer = true;
			appear = false;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag ("Asleep")) {
			print ("collider exit");
			touchingPlayer = false;
			appear = true;
}
	}
}
