﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class GeomCreator : MonoBehaviour 
{
    public List<string> names;
    public GameObject[] prefabs;
    //public GameObject geomPrefab;
    public Transform sourceTransform;
    public Text geomsText;
    private int _geoms = 0;


	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
        /*
	    if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            
            CreateGeom();
        }
        */
	}

    public void CreateGeom(string name)
    {
        int index = names.IndexOf(name);
        Debug.Log(name + ", " + index);
        if (index >= 0 && index < prefabs.Length && sourceTransform != null)
        {
            GameObject geomPrefab = prefabs[index];
            int amountToAdd = 10;
            float dist = 0.8f; //0.1f;
            for (int count = 0; count < amountToAdd; count++)
            {
                GameObject go = Instantiate(geomPrefab, sourceTransform.position + (Random.insideUnitSphere * dist), Quaternion.identity);
                _geoms += 1;
            }
            geomsText.text = "Objects: " + _geoms;
        }
    }
}
