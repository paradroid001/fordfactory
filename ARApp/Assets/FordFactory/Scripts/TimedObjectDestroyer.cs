﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedObjectDestroyer : MonoBehaviour {

    public float timeToLive = 5.0f;
	// Use this for initialization
	void Start () 
    {
	    Destroy(gameObject, timeToLive);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
