﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugButton : MonoBehaviour 
{

    public GameObject explosionPrefab;
	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void OnClick()
    {
        Instantiate(explosionPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        Handheld.Vibrate();
    }
}
