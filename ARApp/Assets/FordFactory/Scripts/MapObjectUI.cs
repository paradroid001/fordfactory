﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class MapObjectInfo
{
    public Sprite flag;
    public string name = "Unknown";
    public string line1 = "Some info goes here";
    public string line2 = "Some more info goes here";
    public string line3 = "Lastly, this is final info";
}

public class MapObjectUI : MonoBehaviour 
{

    private bool _showing;
    public GameObject uiPrefab;
    public Transform uiAttachPoint;
    public MapObjectInfo mapObjectInfo;
    private Vector3 _originalScale;

    private GameObject worldUI;
    private GameObject uiRoot;

	// Use this for initialization
	void Start () 
    {
        worldUI = Instantiate(uiPrefab, uiAttachPoint.position, Quaternion.identity);
        worldUI.transform.SetParent(transform);
        worldUI.GetComponent<MapObjectUIPanel>().SetUI(mapObjectInfo);
        _originalScale = worldUI.transform.localScale;
		worldUI.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (_showing)
        {
            //uiRoot.transform.position = Camera.main.WorldToScreenPoint(transform.position);
	        //uiRoot.transform.LookAt(Camera.main.transform, Vector3.up);
            
            Vector3 camPos = Camera.main.transform.position;
            Vector3 targetPos = new Vector3(camPos.x, worldUI.transform.position.y, camPos.z);
            Debug.DrawLine(transform.position, worldUI.transform.TransformPoint(targetPos), Color.red);

            worldUI.transform.LookAt(targetPos);
            worldUI.transform.Rotate(new Vector3(0, 180, 0), Space.World);
            float dist = Vector3.Distance(worldUI.transform.position, Camera.main.transform.position);
            float newscale = Mathf.Pow(dist,0.5f )/5.0f;
            worldUI.transform.localScale = _originalScale * newscale;
	        //worldUI.transform.LookAt(Camera.main.transform, Vector3.up);
        }
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "MainCamera")
        {
            _showing = true;
            worldUI.SetActive(true);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "MainCamera")
        {
            _showing = false;
            worldUI.SetActive(false);
        }
    }
}
