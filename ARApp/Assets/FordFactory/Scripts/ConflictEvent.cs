﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class EventTime
{
    [Range(1, 31)]
    public int dd = 1;
    [Range(1, 12)]
    public int mm = 1;
    [Range(1941, 1943)]
    public int yyyy= 1942;
    [Range(0, 23)]
    public int h;
    [Range(0, 59)]
    public int m;
    [Range(0, 59)]
    public int s;
    private DateTime _dateTime;


    public DateTime GetTime()
    {
        _dateTime = new DateTime(yyyy, mm, dd, h, m, s);
        return _dateTime;
    }
}

[System.Serializable]
public class EventWaypoint
{
}

[System.Serializable]
public class InsantiateDetails
{
    public Transform transform;
    public GameObject prefab;
}

[System.Serializable]
public class EventTrigger
{
    //public Transform transform;
    public bool triggered = false;
    [Range(0.0f, 1.0f)]
    public float triggerPoint;
    public bool showAtStart = true;
    public bool hideAtEnd = false;
    public List<InsantiateDetails> prefabs;

    public void Reset(GameObject g)
    {
        triggered = false;
        g.SetActive(showAtStart);
    }

    public void Trigger(GameObject g)
    {
        if (triggered)
            return;
        triggered = true;
        foreach (InsantiateDetails details in prefabs)
        {
            GameObject.Instantiate(details.prefab, details.transform.position, details.transform.rotation);
        }
        if (hideAtEnd)
        {
            //Debug.Log("Hide at end!");
            g.SetActive(false);
        }
    }
}

[System.Serializable]
public class EventObject
{
    public BezierSpline spline;
    public bool lookForward;
    public GameObject gameObject;
    public EventTrigger[] triggers;
    
    public void UpdateEvent(float p)
    {
        if (spline != null)
        {
            UpdatePosition(p);
        }
        foreach (EventTrigger trigger in triggers)
        {
            if (p > trigger.triggerPoint && !trigger.triggered)
            {
                trigger.Trigger(gameObject);
            }
        }
        
    }

    public void UpdatePosition(float p)
    {
        //Debug.Log(gameObject.name + "updating for progress " + p);
        Vector3 position = spline.GetPoint(p);
		//gameObject.transform.localPosition = position;
		gameObject.transform.position = position;
		if (lookForward) 
        {
			gameObject.transform.LookAt(position + spline.GetDirection(p));

        }
    }

    public void Reset()
    {
        UpdateEvent(0.0f);
        foreach (EventTrigger trigger in triggers)
        {
            trigger.Reset(gameObject);
        }
    }
}

public class ConflictEvent : MonoBehaviour 
{
    public EventMarker eventMarker;
    public string eventName;
    //public BezierSpline[] paths;
    //public int dd, mm, yyyy;
    public EventTime startTime;
    public EventTime endTime;
    public EventObject[] eventObjects;
    public float desiredDuration = 3.0f;
    private float _eventProgress;
    private float _eventDuration;
	public bool showObjects = false;
    public GameObject objectsRoot;
    


    void Awake()
    {
        TimeSpan _t = endTime.GetTime() - startTime.GetTime();
        _eventDuration = (float)_t.TotalSeconds;
        //Debug.Log("ConflictEventDuration " + eventName + " = " + _duration);
        //_duration = 3.0f;
        if (eventMarker != null)
        {
            eventMarker.SetTime(startTime.GetTime() );
        }
        UpdateShowStatus();
    }
    
    public void Initialise () 
    {
    }
	
    public void UpdateShowStatus()
    {
        objectsRoot.SetActive(showObjects);
        //eventMarker.gameObject.SetActive(showObjects);
        ShowUI(showObjects);
    }

    public void ShowUI(bool show)
    {
        eventMarker.Show(show);
    }

    public void StopEvent()
    {
        //show ui when stopped
        ShowUI(true);
    }

    public void StartEvent()
    {
        //hide ui when starting
        ShowUI(false);
    }

	// Update is called once per frame
	void Update () 
    {
        //if (_duration <= 0.0f)
        //    return;
		foreach (EventObject e in eventObjects)
        {
            /*
            _eventProgress += Time.deltaTime / _duration;
            if (_eventProgress > 1f)
                _eventProgress = 1f;
            */
            e.UpdateEvent(_eventProgress);
        }
	}

    public void SetProgress(float val, TimeOfDay tod)
    {
        _eventProgress = val;
        tod.AdjustTimeTo(startTime.GetTime().AddSeconds( (_eventDuration * val) ) );
    }

    public float GetScaledDuration()
    {
        return _eventDuration / desiredDuration;
    }

    public void Reset()
    {
        foreach (EventObject e in eventObjects)
        {
            e.Reset();
        }

    }
}
