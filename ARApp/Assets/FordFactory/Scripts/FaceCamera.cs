﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour 
{

    private GameObject worldUI;
    private Vector3 _originalScale;
    public bool dynamicScale = false;

	// Use this for initialization
	void Start () 
    {
		worldUI = gameObject;
        _originalScale = worldUI.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () 
    {

            //uiRoot.transform.position = Camera.main.WorldToScreenPoint(transform.position);
	        //uiRoot.transform.LookAt(Camera.main.transform, Vector3.up);
            
            Vector3 camPos = Camera.main.transform.position;
            Vector3 targetPos = new Vector3(camPos.x, worldUI.transform.position.y, camPos.z);
            Debug.DrawLine(transform.position, worldUI.transform.TransformPoint(targetPos), Color.red);

            worldUI.transform.LookAt(targetPos);
            worldUI.transform.Rotate(new Vector3(0, 180, 0), Space.World);
            if (dynamicScale)
            {
                float dist = Vector3.Distance(worldUI.transform.position, Camera.main.transform.position);
                float newscale = Mathf.Pow(dist,0.5f )/5.0f;
                worldUI.transform.localScale = _originalScale * newscale;
	            //worldUI.transform.LookAt(Camera.main.transform, Vector3.up);
            }
	}
}
