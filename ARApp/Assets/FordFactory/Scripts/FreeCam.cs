﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class FreeCam : MonoBehaviour 
{
    public float speed = 0.2f;
	public GameObject ARCameraRoot;
    public DefaultTrackableEventHandler imageTarget;
    public float sensitivityX = 1.0f;
    public float sensitivityY = 1.0f;
    private Camera _camera;

    void Awake()
    {
        if (ARCameraRoot != null)
        {
            ARCameraRoot.SetActive(false);
        }
        if (imageTarget != null)
        {
            imageTarget.enabled = false;
        }
        _camera = GetComponent<Camera>();
    }

    // Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 offset = new Vector3();

	    if (Input.GetKey(KeyCode.W) )
        {
            offset += new Vector3(0, 0, 1);   
        }
        if (Input.GetKey(KeyCode.S) )
        {
            offset += new Vector3(0, 0, -1);   
        }
        if (Input.GetKey(KeyCode.D) )
        {
            offset += new Vector3(1, 0, 0);   
        }
        if (Input.GetKey(KeyCode.A) )
        {
            offset += new Vector3(-1, 0, 0);   
        }
        transform.position += offset * speed * Time.deltaTime;
        
        //Vector2 mdiff = new Vector3( (Input.mousePosition.x - (Screen.width/2.0f)) * sensitivityX, (Input.mousePosition.y-(Screen.height/2.0f)) * sensitivityY );
        //transform.LookAt(transform.position + new Vector3(mdiff.x, mdiff.y, 3.0f) );
	}
}
