﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayButtonGraphic : MonoBehaviour 
{
    Image _playButton;
    public Sprite playSprite;
    public Sprite pauseSprite;
    public ConflictEventManager conflictEventManager;
	// Use this for initialization
	void Start () 
    {
		_playButton = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        if (conflictEventManager.IsPlaying() )
        {
            _playButton.sprite = pauseSprite;
        }
        else
            _playButton.sprite = playSprite;
    }
}
