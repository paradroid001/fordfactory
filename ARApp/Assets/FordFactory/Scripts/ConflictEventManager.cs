﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class ConflictEventManager : MonoBehaviour {

    public Slider scrubber;
    public GameObject controls;
    public ConflictEvent[] conflictEvents;
    public TimeOfDay timeOfDay;
    public Text eventName;
    private int _currentEventIndex = 0;
    private ConflictEvent _currentEvent;
    private bool _playing = false;
    public int playSpeed = 30; //30 minutes per second
    private float _dtaccumulator;
    private float _dtthreshold;
    private float _currentEventDuration;
    private float _currentEventAccumulator;
    public bool showScrubber = false;
    public AudioMixer audioMixer;
    private float _eventMixerInitialValue;
    private CanvasGroup _canvasGroup;
    public float controlsFadeTime = 2.0f;
    public float minFadeValue = 0.1f;

    void Awake()
    {
        audioMixer.GetFloat("EventAudioVolume", out _eventMixerInitialValue);
        _currentEventIndex = 0;
        _canvasGroup = GetComponent<CanvasGroup>();
        //Amount needed to increase one second of AR time.
        //_dtthreshold = 1.0f / (60.0f * playSpeed);
    }

	// Use this for initialization
	void Start () 
    {
        foreach (ConflictEvent c in conflictEvents)
        {
            c.Initialise();
        }
	    SwitchToEvent(_currentEventIndex);
	}

    public ConflictEvent CurrentEvent
    {
        get{return _currentEvent;}
    }

    void MuteEventAudio(bool mute)
    {
        float newvol = _eventMixerInitialValue;
        if (mute)
        {
            newvol = -80.0f;
        }
        audioMixer.SetFloat("EventAudioVolume", newvol);
    }
	
	// Update is called once per frame
	void Update () 
    {

	    if (_playing)
        {
            //showScrubber = true;
            //_dtaccumulator = 0;
            //if (_dtaccumulator > _dtthreshold)
            //{
                //how many seconds do we advance?
                //int simulatorseconds = (int)(_dtaccumulator/_dtthreshold);
                _currentEventAccumulator +=  Time.deltaTime;
                if (_currentEventAccumulator < _currentEventDuration)
                {
                    //TimeSpan t = new TimeSpan(0, 0, simulatorseconds);
                    //timeOfDay.AdjustTimeBy(t);
                    scrubber.value = _currentEventAccumulator/_currentEventDuration;
                    _currentEvent.SetProgress(_currentEventAccumulator/_currentEventDuration, timeOfDay);
                }
                else
                {
                    Debug.Log("Stopped event");
                    Stop();
                }
                //_dtaccumulator = 0.0f;
            //}
        
        

            if (showScrubber)
            {
                _dtaccumulator += Time.deltaTime;
                _canvasGroup.alpha = Mathf.Max(controlsFadeTime - (_dtaccumulator), minFadeValue);
                if (_dtaccumulator > controlsFadeTime)
                {
                    //showScrubber = false;
                    _dtaccumulator = controlsFadeTime;
                }
            }
        }
        else
        {
            //When stopped, show the scrubber!
            showScrubber = true;
            _canvasGroup.alpha = 1.0f;
            _dtaccumulator = 0.0f;
        }

        //Check for touch only if not showing scrubber.
        if (!showScrubber && (Input.touchCount > 0 || Input.GetMouseButton(0)) )
        {
            showScrubber = true;
            _canvasGroup.alpha = 1.0f;
            _dtaccumulator = 0.0f;
        }
        if (!showScrubber)
        {
            _canvasGroup.alpha = 0.0f;
        }


        //scrubber.gameObject.SetActive(showScrubber);
        //controls.SetActive(showScrubber);
	}

    public void SwitchToEvent(int index)
    {
        _currentEvent = conflictEvents[_currentEventIndex];
        _currentEvent.Reset();
        _currentEvent.showObjects = true;
        _currentEvent.UpdateShowStatus();
        timeOfDay.AdjustTimeTo(_currentEvent.startTime.GetTime() );
        eventName.text = _currentEvent.eventName;
        _currentEventDuration = _currentEvent.desiredDuration;//_currentEvent.GetScaledDuration(playSpeed * 60);
        Debug.Log("Duration for " + _currentEvent.eventName + " = " + _currentEventDuration);
        scrubber.minValue = 0;
        scrubber.maxValue = 1.0f;
        _currentEventAccumulator = 0;
        scrubber.value = _currentEventAccumulator;
    }

    public void NextEvent()
    {
        Stop();
        FindObjectOfType<PlayButtonGraphic>().OnClick();//refresh play button state
        Debug.Log("Next Event");
        _currentEvent.showObjects = false;
        _currentEvent.UpdateShowStatus();

        _currentEventIndex += 1;
        if (_currentEventIndex >= conflictEvents.Length)
            _currentEventIndex = 0;
        SwitchToEvent(_currentEventIndex);
    }

    public void PrevEvent()
    {
        Stop();
        FindObjectOfType<PlayButtonGraphic>().OnClick();//refresh play button state
        Debug.Log("Prev Event");
        _currentEvent.showObjects = false;
        _currentEvent.UpdateShowStatus();

        _currentEventIndex -= 1;
        if (_currentEventIndex < 0)
            _currentEventIndex = conflictEvents.Length-1;
        SwitchToEvent(_currentEventIndex);
    }

    public void OnPlay()
    {
        if (_playing)
            Stop();
        else
            Play();
    }

    public void Play()
    {
        _currentEvent.StartEvent();
        MuteEventAudio(false);
        _playing = true;
        //initialise the event accumulator in case we're partway scrubbed.
        _currentEventAccumulator = _currentEventDuration * scrubber.value;
    }

    public void Stop()
    {
        _currentEvent.StopEvent();
        MuteEventAudio(true);
        _playing = false;
    }

    public void OnScrub(float val)
    {
        //Debug.Log("OnScrub " + val);
        _currentEvent.SetProgress(val, timeOfDay);
    }

    public bool IsPlaying()
    {
        return _playing;
    }

    public void ShowCurrentUI(bool show)
    {
        showScrubber = show;
        if (_currentEvent != null)
            _currentEvent.ShowUI(show);
    }

}
