﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmissionStopper : MonoBehaviour {

    public float timeToEmit = 3.0f;
    public ParticleSystem pfx;
	// Use this for initialization
	void Start () 
    {
        if (pfx == null)
        {
            pfx = GetComponent<ParticleSystem>();
        }
		Invoke("StopEmission", timeToEmit);
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    void StopEmission()
    {
        if (pfx != null)
        {
            var em = pfx.emission;
            em.enabled = false;
        }
    }
}
