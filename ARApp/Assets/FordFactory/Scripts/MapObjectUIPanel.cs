﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapObjectUIPanel : MonoBehaviour 
{

    public Text textTitle;
    public Text textLine1;
    public Text textLine2;
    public Text textLine3;
    public Image flag;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetUI(MapObjectInfo moi)
    {
        textTitle.text = moi.name;
        textLine1.text = moi.line1;
        textLine2.text = moi.line2;
        textLine3.text = moi.line3;
        flag.sprite = moi.flag;
    }
}
