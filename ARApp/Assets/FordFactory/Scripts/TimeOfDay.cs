﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TimeOfDay : MonoBehaviour 
{
    
    public GameObject sun;
    public Text dateText;
    public Text ddText;
    public Text mmmText;
    public Text yyyyText;
    
    private bool _isMouseDragging = false;
    private Vector2 _mousePos;
    private Vector2 _previousMousePos;
    //private float _distThisDrag = 0.0f;
    private DateTime _dateTime;

	// Use this for initialization
	void Start () 
    {
        _dateTime = new DateTime(1942, 1, 1, 8, 0, 0);	
        //dateText.text = _dateTime.ToString();
        SetSceneLightingByTime(_dateTime.Hour, _dateTime.Minute);
        UpdateDateText();
	}

    public void UpdateDateText()
    {
        //dateText.text = _dateTime.ToString("hh") + ":" +_dateTime.ToString("mm") + ":" + _dateTime.ToString("ss");
        dateText.text = _dateTime.ToString("hh:mm tt");
        ddText.text = _dateTime.ToString("dd");
        mmmText.text = _dateTime.ToString("MMM");
        yyyyText.text = _dateTime.ToString("yyyy");
    }
	
	// Update is called once per frame
	void Update () 
    {
        /*
        if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            Vector2 drag = CheckDragged();
            //Change the time of day / date
            TimeSpan t = new TimeSpan((int)drag.x, 0, 0);
            _dateTime += t;

            UpdateDateTime();
        }

        if (!Input.GetMouseButton(0) )
        {
            _isMouseDragging = false;
        }
        */
	}

    public void AdjustTimeTo(DateTime d)
    {
        //TimeSpan t = new TimeSpan(_dateTime - d);
        //_dateTime += t;
        _dateTime = d;
        UpdateDateTime();
    }

    public void AdjustTimeBy(TimeSpan t)
    {
        _dateTime += t;
        UpdateDateTime();
    }

    private void UpdateDateTime()
    {
        //dateText.text = _dateTime.ToString();
        SetSceneLightingByTime(_dateTime.Hour, _dateTime.Minute);
        UpdateDateText();
    }

    Vector2 CheckDragged()
    {
        Vector2 retval = new Vector2(0.0f, 0.0f);
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) 
        {
            // Get movement of the finger since last frame
            retval = Input.GetTouch(0).deltaPosition;
        }

        if (Input.GetMouseButton(0) )
        {
            if (!_isMouseDragging)
            {
                Debug.Log("Mouse Down");
                //initialise mouse dragging
                _mousePos = Input.mousePosition;
                _previousMousePos = _mousePos;
                _isMouseDragging = true;
            }
            else
            {
               _previousMousePos = _mousePos;
               _mousePos = Input.mousePosition;
            }
            retval = _mousePos - _previousMousePos;
        }
        return retval;
    }

    void OnMouseDown()
    {
        Debug.Log("OnMouseDOwn");
    }

    void OnMouseDrag()
    {
        Debug.Log("Mouse Drag");
    }

    /* hour 0 = 270 degrees
     * hour 6 = 180 degrees
     * hour 12 = 90
     * hour 18 = 0 degrees = sunset
     */
    void SetSceneLightingByTime(int hour, int min)
    {
        Vector3 v = sun.transform.rotation.eulerAngles;
        Quaternion newrot = Quaternion.Euler( (((hour+min/60.0f) / 24.0f) * 360.0f) -90.0f, 90.0f, 0.0f); 
        sun.transform.rotation = newrot;
    }

    /*
    public void BeginDrag()
    {
        _isDragging = true;
        _distThisDrag = 0.0f;
    }

    public void EndDrag()
    {
        _isDragging = false;

    }
    */

}
