﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(ConflictEvent))]
public class ConflictEventInspector : Editor
{
    /*
    private void OnSceneGUI()
    {
        
        ConflictEvent conflictEvent = target as ConflictEvent;
        Transform handleTransform = conflictEvent.transform;
        Quaternion handleRotation = Tools.pivotRotation == PivotRotation.Local ?
            handleTransform.rotation : Quaternion.identity;	

        //EventObject[] eventObjects = conflictEvent.eventObjects;
        foreach (EventObject eo in conflictEvent.eventObjects)
        {
            int numWaypoints = eo.waypoints.Length;
            Vector3[] linePoints = new Vector3[numWaypoints * 2];
            Vector3[] changedPoints = new Vector3[numWaypoints];

            int index = 0;
            //Check for changes
            EditorGUI.BeginChangeCheck();
            for (int i = 0; i < numWaypoints-1; i++)
            {
                if (eo.waypoints[i] != null && eo.waypoints[i+1] != null)
                {
                    //linePoints[index] = handleTransform.TransformPoint(eo.waypoints[i].transform.position);
                    //linePoints[index+1] = handleTransform.TransformPoint(eo.waypoints[i+1].transform.position);
                    linePoints[index] = eo.waypoints[i].transform.position;
                    linePoints[index+1] = eo.waypoints[i+1].transform.position;
                    
                    //draw a handle
                    changedPoints[i] = Handles.DoPositionHandle(linePoints[index], handleRotation);
                    Handles.Label( linePoints[index] + Vector3.up * 2, linePoints[index].ToString( ) );
                    changedPoints[i+1] = Handles.DoPositionHandle(linePoints[index+1], handleRotation);
                    Handles.Label( linePoints[index+1] + Vector3.up * 2, linePoints[index+1].ToString( ) );
                    index+=2;

                }   
            }

            Handles.color = Color.white;
            Handles.DrawDottedLines(linePoints, 4);

            //Allow Changes
            if (EditorGUI.EndChangeCheck())
            {
                for (int i = 0; i<numWaypoints; i++)
                {
                    eo.waypoints[i].transform.position = changedPoints[i];
                }
            }

        }
    }
    */
}
