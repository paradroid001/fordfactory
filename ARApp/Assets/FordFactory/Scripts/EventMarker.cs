﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class EventMarker : MonoBehaviour {

    public Image background;
    public Text monthYear;
    public Text day;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Show(bool show)
    {
        Debug.Log("Showing Event Marker " + name);
        background.gameObject.SetActive(show);
    }

    public void SetTime(DateTime d)
    {
        monthYear.text = d.ToString("MMM") + " '" +  d.ToString("yy");//"Feb '42";
        day.text = d.ToString("dd");
    }
}
