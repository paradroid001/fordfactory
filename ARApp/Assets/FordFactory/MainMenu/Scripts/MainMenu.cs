﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class MainMenu : MonoBehaviour 
{
    
    public Canvas mainMenuCanvas;
    public GameObject mainMenuPanel;
    public GameObject loadingScreen;
    public GameObject backPanel;
    public GameObject titlePanel;
    public GameObject lostTrackingPanel;
    
    private static MainMenu _instance;
    private bool _showing = false;

    public static MainMenu obj
    {
        get 
        { 
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<MainMenu>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            GameObject.Destroy(this.gameObject);
        }
        else
        {
            //force the singleton to be instantiated
            //by accessing the getter.
            MainMenu m = obj;
            SceneManager.activeSceneChanged += OnSceneChanged;
        }
    }

    
	// Use this for initialization
	void Start () 
    {
	    if (_showing)
            Show(true);
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (Input.GetButtonDown("Cancel") )
        {
            Toggle();
        }
	}

    public void Toggle()
    {
        Show(!_showing);
    }

    public void Show(bool shouldshow)
    {
        ShowBackPanel(false);
        _showing = shouldshow;
        mainMenuPanel.SetActive(shouldshow);
        lostTrackingPanel.SetActive(!shouldshow);
    }

    public void ShowBackPanel(bool shouldShow)
    {
        backPanel.SetActive(shouldShow);
        titlePanel.SetActive(!shouldShow);
    }

    public void ShowLostTrackingPanel(bool shouldshow)
    {
        lostTrackingPanel.SetActive(shouldshow);
        //mainMenuPanel.SetActive(!shouldshow);
    }

    public void LoadScene(string scenename)
    {
        if (scenename != "")
        {   
            Show(false);
            ShowBackPanel(false);
            loadingScreen.SetActive(true);
            SceneManager.LoadScene(scenename);
        }

    }

    public void OnSceneChanged(Scene from, Scene to)
    {
        loadingScreen.SetActive(false);
    }

    public void OnQuit()
    {
        Debug.Log("on quit");
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }


}
